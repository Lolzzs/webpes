// global variables
const url = 'https://jsonplaceholder.typicode.com/posts?_page=';
let actualUrl = '';

// functions
function getPosts(pageUrl) {

  let output = '';
  let navigationLinks = undefined;
  let navigation= '';

  fetch(pageUrl)
    .then((res) => {
      navigationLinks = parseLinkHeader(res.headers.get('link'));
      actualUrl = pageUrl;
      return res.json()
    })
    .then((data) => {

      if (data) {

        data.forEach(post => {

          output += `
            <div>
                <h3 class="__postTitle" onclick="getUsers(${post.userId});">${post.title}</h3>
                <p>${post.body}</p>
            </div>
            `;         
          
        });
        
        createPagination(navigationLinks,actualUrl);
      }

      document.getElementById('output').innerHTML = output;
      
    })
    .catch(error => console.error(error));
}

function getPageNumberFromUrl(url) {
  return url.split('_page=').pop().split('&').shift();
}

function createPagination(navigationLinks,actualUrl) {
  if (getPageNumberFromUrl(actualUrl) == getPageNumberFromUrl(navigationLinks.first)) {
    navigation = `
    <div class="__navigation">
      <a class="__actual-page" onclick="getPosts('${navigationLinks.first}');"><< začátek</a>`;
  } else {
    navigation = `
    <div class="__navigation">
      <a onclick="getPosts('${navigationLinks.first}');"><< začátek</a>`;
  }

  if (navigationLinks.prev) {
    navigation += `
      <a onclick="getPosts('${navigationLinks.prev}');">< posun o jednu stránku</a>`;
  }

  let last = getPageNumberFromUrl(navigationLinks.last);
  let pages = '';
  for (let i = 2; i < last; i++) {

    if (getPageNumberFromUrl(actualUrl) == i) {
      pages += `<a class="__actual-page" onclick="getPosts('${url}${i}');">${i}</a>`
    } else {
      pages += `<a onclick="getPosts('${url}${i}');">${i}</a>`;
    }
  }

  navigation += pages += '';

  if (navigationLinks.next) {
    navigation += `
      <a onclick="getPosts('${navigationLinks.next}');">posun o jednu stránku ></a>`;
  }

  if (getPageNumberFromUrl(actualUrl) == getPageNumberFromUrl(navigationLinks.last)) {
    navigation += `<a class="__actual-page" onclick="getPosts('${navigationLinks.last}');">konec >></a></div>`;
  } else {
    navigation += `<a onclick="getPosts('${navigationLinks.last}');">konec >></a></div>`;
  }

  document.getElementById('navigation').innerHTML = navigation;
}


// implemented solution from https://gist.github.com/deiu/9335803 (link parser)
function parseLinkHeader(link) {

  var linkexp = /<[^>]*>\s*(\s*;\s*[^\(\)<>@,;:"\/\[\]\?={} \t]+=(([^\(\)<>@,;:"\/\[\]\?={} \t]+)|("[^"]*")))*(,|$)/g;
  var paramexp = /[^\(\)<>@,;:"\/\[\]\?={} \t]+=(([^\(\)<>@,;:"\/\[\]\?={} \t]+)|("[^"]*"))/g;

  var matches = link.match(linkexp);
  var res = {};

  for (var i = 0; i < matches.length; i++) {
      var split = matches[i].split('>');
      var href = split[0].substring(1);
      var ps = split[1];
      var s = ps.match(paramexp);
      for (var j = 0; j < s.length; j++) {
          var p = s[j];
          var paramsplit = p.split('=');
          var name = paramsplit[0];
          var rel = paramsplit[1].replace(/["']/g, '');
          res[rel] = href;
      }
  }

  return res;
}
  
  
function getUsers(idUser) {

  let output; 
    
  fetch('https://jsonplaceholder.typicode.com/users/' + idUser)
    .then((res) => res.json())
    .then((data) => {
      
      if (data) {

        document.getElementById('navigation').classList.add('is-not-displayed');

        output = `
        <div>
          <h3>${data.name}</h3>
          <p>${data.email}</p>
          <a class="__back" href="index.html">zpět</a>
        </div>`; 

        document.getElementById('output').innerHTML = output;

      }
                
    })
    .catch(error => console.error(error));
}
